from random import randint

name = input("\nHi! What is your name? ")
print("\nI am going to guess your birthday " + name + ".")

for guess_num in range(1,6):
  random_month = randint(1,12)
  random_year = randint(1924,2004)

  print(f'\nGuess {guess_num}: Is your bithday {random_month} / {random_year}?')
  guess = input('\nyes or no? ')

  if guess == 'yes':
    print('\nI knew it!\n')
    exit()
  elif guess == 'no':
    print('\nDrat! Lemme try again!')
    if guess_num == 5:
      print('\nI have other things to do. Good bye.\n')